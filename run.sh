#!/bin/bash

cp ./dockercompose_file/docker-compose.yml ./

workinDir=$(pwd)
mountpoint_src="/mnt/sources"
mountpoint_mysql="/var/lib/mysql"

echo -e "Scegli l'operazione da fare:\n
1) Build PROD\n
2) Build TEST\n
3) Build DEV\n 
4) Build ed esecuzione DEV\n 
5) Build ed esecuzione TEST\n 
"
read selection

case $selection in
    1 ) cd $(pwd)/Certificatore/sorgenti
        npm run build_prod
        cd ../../EthereumEndpoint/sorgenti
        npm run build_prod
        ;;
    2 ) cd $(pwd)/Certificatore/sorgenti
        npm run build_test
        ;;
    3 ) cd $(pwd)/Certificatore/sorgenti
        npm run build_dev
        cd ../../EthereumEndpoint/sorgenti
        npm run build_dev
        ;;
    4 ) cd $(pwd)/Certificatore/sorgenti
        npm install
        cd ../../EthereumEndpoint/sorgenti
        npm install
        cd ../..
        sed -i "/      - CHANNEL_BUILD=mychannel/c\      - CHANNEL_BUILD=dev" ./docker-compose.yml
        sed -i "/- my-iir-source-repo/c\      - $workinDir:$mountpoint_src/" ./docker-compose.yml
        sed -i "/- my-iir-mysql_db/c\      - $workinDir/mysql_db:$mountpoint_mysql/" ./docker-compose.yml
        docker-compose up   
        ;;
    5 ) cd $(pwd)/Certificatore/sorgenti
        npm install
        cd ../..
        sed -i "/      - CHANNEL_BUILD=mychannel/c\      - CHANNEL_BUILD=test" ./docker-compose.yml
        sed -i "/- my-iir-source-repo/c\      - $workinDir:$mountpoint_src/" ./docker-compose.yml
        sed -i "/- my-iir-mysql_db/c\      - $workinDir/mysql_db:$mountpoint_mysql/" ./docker-compose.yml
        docker-compose up
        ;;
    * ) echo "Opzione non presente"
        
esac
