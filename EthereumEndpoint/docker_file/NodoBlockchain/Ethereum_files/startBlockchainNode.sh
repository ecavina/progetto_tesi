#!/usr/bin/env bash
#read -p "Inserire il nome del nodo della blockchain: " nomeNodo
#read -p "Inserire l'ip del nodo: " ipNodo
ipnodo="$(ifconfig eth0 | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)"
geth --password /root/Ethereum/ethPassword account new
geth --password /root/Ethereum/ethPassword account new
geth --identity "nodo_master"  --mine --rpc --rpcport "8000" --rpccorsdomain "*" --port "30303" --rpcapi "db,eth,net,personal,web3" --networkid 1900 --nat "any" --rpcaddr $ipnodo
echo "INIZIALIZZAZIONE NODO COMPLETATA!"
