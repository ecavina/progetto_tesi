#!/bin/bash
#
# inizializzazione server
#

ip=iir-ethereum

echo "Initializing environment...."

cd /mnt/sources/EthereumEndpoint/sorgenti/
npm run build_dev

# attendo che il servizio sia disponibile sulla porta 8001
until nc -z -v -w30 $ip 8000
do 
  echo "Waiting for ethereum connection...." 
  sleep 5 
done 

cd /mnt/sources/EthereumEndpoint/sorgenti/build/
node app.js

#cd /root/eth-endpoint/
#node app.js
