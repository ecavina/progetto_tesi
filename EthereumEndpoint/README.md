##**EthereumEndpoint**

Endpoint NodeJS su cui vengono richiamati i comandi per interagire con la Blockchain Ethereum.

Espone dei servizi REST per la certificazione e la ricerca delle transazioni su Blockchain.

Questo modulo necessità di collegarsi ad un nodo attivo di Ethereum per l'esecuzione. Nell'esecuzione automatica viene infatti istanziato preventivamente il nodo Ethereum rispetto a questo modulo.

L'esecuzione dell'ambiente prevede l'esecuzione del nodo Ethereum tramite Docker. Il docker file corrispondente e i file necessari si trovano all'interno di:
*docker_file/NodoBlockchain/*
