'use strict';

var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();
module.exports = app; // for testing
var globalVariable = require("./config/globalVariable.js");
var logger = globalVariable.logger;

var config = {
  appRoot: __dirname // required config
};

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  // install middleware
  swaggerExpress.register(app);

  var port = globalVariable.port || 10010;
  app.use(logRequest);
  app.listen(port);
  logger.info('Server is running on port: ' + port);
});

//Runs every time a request is received
function logRequest(req, res, next) {
    logger.info('Request from: ' + req.ip + ' For: ' + req.path);
    next();
}
