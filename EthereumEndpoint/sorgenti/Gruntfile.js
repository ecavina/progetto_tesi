module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    devSettings: grunt.file.readJSON('gruntConfig/gruntDevConfig.json'),
    prodSettings: grunt.file.readJSON('gruntConfig/gruntProdConfig.json'),
    config: {
      dev: {
        options: {
          variables: {
            nodePort: '<%- devSettings.nodePort %>',
            pwEth:'<%- devSettings.pwEth %>',
            urlBlockchain:'<%- devSettings.urlBlockchain %>'
          }
        }
      },
      prod: {
        options: {
          variables: {
            nodePort: '<%- prodSettings.nodePort %>',
            pwEth:'<%- prodSettings.pwEth',
            urlBlockchain: '<%- prodSettings.urlBlockchain %>'
          }
        }
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, src: [
            'api/**/*.*',
            'config/*.*',
            'test/**/*.*',
            'logs/*.*',
            'node_modules/**/*.*',
            'app.js',
            'package.json',
            'scripts/*.*'
          ], dest: 'build/'},
        ],
      },
    },
    replace: {
      globalVar: {
        options: {
          patterns: [{
              match: 'nodePort',
              replacement: '<%= grunt.config.get("nodePort") %>'
          },
          {
            match: 'pwEth',
            replacement: '<%= grunt.config.get("pwEth") %>'
          },
          {
            match: 'urlBlockchain',
            replacement: '<%= grunt.config.get("urlBlockchain") %>'
          }]
        },
        src:'config/globalVariable.js',
        dest:'build/config/globalVariable.js'
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-config');
  grunt.loadNpmTasks('grunt-include-source');
  grunt.registerTask('build_dev', ['config:dev', 'copy:main', 'replace']);
  grunt.registerTask('build_prod', ['config:prod', 'copy:main', 'replace']);
  grunt.registerTask('build_compose', ['config:compose', 'copy:main', 'replace']);
}
