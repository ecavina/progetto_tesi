'use strict';

var ethUtility = require('./../helpers/ethereumUtility.js').init();
var globalVariable = require("./../../config/globalVariable.js");
var logger = globalVariable.logger;

function register(req, res, next) {
    var hashData = req.body.hashData;
    var hashDocument = req.body.hashImage;
    var timestamp = new Date();

    ethUtility.createTransaction("SHA-1", hashData, hashDocument, 1, timestamp)
    .then(function(result) {
      res.status(200).send({id:result});
    })
    .catch(function(error) {
	logger.error(error);
      res.status(500).send({message:"Errore Server"});
    });
}

function unregister(req, res, next) {
  var hashData = req.body.hashData;
  var hashDocument = req.body.hashDocument;
  var timestamp = new Date();

  ethUtility.createTransaction("SHA-1", hashData, hashDocument, 0, timestamp)
  .then(function(result) {
    res.status(200).send({id:result});
  })
  .catch(function(error) {
	logger.error(error);
    res.status(500).send({message:"Errore Server"});
  });
}

function search(req, res, next) {
  var tx = req.swagger.params.id.value;
  ethUtility.getTransaction(tx)
  .then(function(result) {
    res.status(200).send({input:result});
  })
  .catch(function() {
    res.status(500).send({message:"Errore Server"});
    return;
  });
}

module.exports = {register, unregister, search}
