All'interno di questa directory vi sono tutti i controller a cui vengono inoltrate le richieste da swagger

**registrations.js**

  Contiene tre funzioni che gestiscono rispettivamente ogni servizio esposto REST (certificazione, rimozione certificazione, ricerca transazione)

  Utilizza la classe /helpers/ethereumUtility.js per comunicare con la Blockchain
