var Web3;
var globalVariable;
var ethConnection;
var ethObject;
var fromAccount;
var toAccount;

exports.init = function() {
  Web3 = require('web3');
  globalVariable = require('./../../config/globalVariable.js');
  logger = globalVariable.logger;

  ethConnection;
  ethObject;
  fromAccount;
  toAccount;
  logger.info("Iniziliazzazione Ethereum Utility");
  web3Object = new Web3();
  web3Object.setProvider(new web3Object.providers.HttpProvider(globalVariable.urlBlockchain));
  ethObject = web3Object.eth;
  fromAccount = ethObject.accounts[0];
  toAccount = ethObject.accounts[1];

  return this;
}

function unlockSender() {
  logger.debug("Sblocco account blockchain");
  web3Object.personal.unlockAccount(fromAccount, globalVariable.pwdAccount);
}

exports.createTransaction = function createTransaction(alg, data, image, flag, timestamp) {
  return new Promise(function(resolve, reject) {
    unlockSender();
    logger.debug("Creazione trasanzione...");
    txId = ethObject.sendTransaction({
        from: fromAccount,
        to: toAccount,
        data: web3Object.toHex(
          {   Algoritmo: alg,
              Data: data,
              Document: image,
              Flag: flag,
              timestamp: timestamp
          }),
        value:  web3Object.toWei(0.1, "ether")
    }, function(error, result) {
      if(error) {
        logger.error("Errore durante invio transazione: "+error);
        reject("Fallito");
      } else {
        logger.info("Transazione completata, id transazione: " + result);
        resolve(result);
      }
    });
  });
}

exports.getTransaction = function getTransaction(tx) {
  return new Promise(function(resolve, reject) {
    var transaction = ethObject.getTransaction(tx, function(error, result) {
      if(error) {
        logger.error("Errore durante la ricerca della transazione: " + error);
        reject("Fallito");
      } else {
        logger.debug("Transazione trovata, conversione ad ascii");
        if(result.input==null) {
          logger.error("Campo input della transazione nullo");
          reject("Fallito");
        }
        var jsonTx=web3Object.toAscii(result.input);
        logger.info("Trovata transazione, campo input al suo interno: " + jsonTx);
        resolve(jsonTx);
      }
    });
  });
}
