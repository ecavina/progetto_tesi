module.exports = {
  urlBlockchain : "@@urlBlockchain",//ip del nodo blockchain
  port: "@@nodePort", //porta del nodejs endpoint
  pwdAccount : "@@pwEth", //password degli account sul nodo blockchain
  logger : getLogger(),
}

function getLogger() {
  var log4js = require('log4js');
	log4js.loadAppender('file');
	log4js.addAppender(log4js.appenders.file('logs/server.log'), 'file');
	var logger = log4js.getLogger('file');
	logger.setLevel('DEBUG');
  return logger;
}
