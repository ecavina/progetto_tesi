

###Configurazione per gli ambienti:

*gruntConfig/gruntDevConfig.json
  gruntConfig/gruntProdConfig.json
  gruntConfig/gruntTestConfig.json
  gruntConfig/gruntComposeConfig.json*

###Buildare il progetto:

  *npm run build_dev
  npm run build_prod
  npm run build_test
  npm run build_compose*

------------

###Editing per i servizi REST:

*swagger project edit* -> per utilizzare la gui di swagger nel browser per modificare l'interfaccia rest e vedere la documentazione dei servizi

------------

###Esecuzione del server

All'interno di *./build/*:

Esecuzione del server: *node app.js*
