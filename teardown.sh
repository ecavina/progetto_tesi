#!/bin/bash
#
# script per la cancellazione di immagini, volumi e reti create da docker
# durante l'istanziazione dell'ambiente.
#

# recupero gli ID dei container a partire dalla parte invariante del nome logico (*iir-<container>) NB: l'indice 0 dell'array corrisponde all'ID del container
str="$(docker images | grep iir-validator)"
array=($str)
validator=${array[0]}

str="$(docker images | grep iir-ethereum)"
array=($str)
ethereum=${array[0]}

str="$(docker images | grep iir-mysql)"
array=($str)
mysql=${array[0]}

str="$(docker images | grep iir-eth-endpoint)"
array=($str)
ethEndpoint=${array[0]}

# recupero l'ID del volume di mysql
str="$(docker volume ls | grep iir-mysql_db)"
array=($str)
db=${array[1]}

# distruzione ambiente
echo "Destroying environment...."
docker-compose down

# cancellazione immagini docker
echo "Destroying images...." 
docker rmi $validator $ethereum $mysql $ethEndpoint

# cancellazione volume database
echo "Destroying volume $db...."
docker volume rm $db

#cancellazione network docker
# recupero l'ID della rete creata ad hoc
str="$(docker network ls | grep docker-imolainrete)"
array=($str)
net=${array[0]}

echo "Destroying network...."
if [[ $net = *[!\ ]* ]]; then
  docker network rm $net
else
  echo "Network has already been removed by docker-compose...."
fi

echo "Deleting dockerfile"
rm docker-compose.yml
