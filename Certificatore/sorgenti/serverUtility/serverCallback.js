module.exports = {
  db_get_mail : function(result, res) {
	   if (result == 0) {
       res.send({response:"false"});
       return;
  	  } else {
	       res.send({response:"true"});
        return;
  	  }
  },

  db_post : function(result, res) {
    if (result == 0) {
      res.send({response:"false"});
      return;
  	} else {
      res.send({response:"true"});
      return;
    }
  },

  get_registration : function(result, res) {
    res.send({input:result});
  },

  post_registration : function(txId, res) {
    res.send({result:txId});
  }
}
