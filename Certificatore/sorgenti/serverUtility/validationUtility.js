var globalVariable = require(__dirname + '/../config/globalVariable.js');
module.exports = {
  validate_email : function(emailAddress) {
  	var pattern = new RegExp(globalVariable.regexpMail);
  	return pattern.test(emailAddress);
  }
}
