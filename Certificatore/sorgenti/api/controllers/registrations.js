'use strict'
var requestify = require('requestify');
var globalVariable = require(__dirname + '/../../config/globalVariable.js');
var errorMessage = require(__dirname + '/../../config/errorMessage.js');
var serverCallback = require(__dirname + '/../../serverUtility/serverCallback.js');
var validator = require(__dirname + '/../../serverUtility/validationUtility.js');
var errorMessage = require(__dirname + '/../../config/errorMessage.js');
var dbUtility = require(__dirname + '/../../db/index.js');
var logger = globalVariable.logger;

function certificateUser(req, res, next) {
  if(req.body.zone && req.body.docType && req.body.userID && req.body.userMail && req.body.name,req.body.surname && req.body.birthday && req.body.numInst && req.body.docNumber && req.body.scadenzaDoc && req.body.photoByte && req.body.dataPack && req.body.hashData && req.body.hashDocument) {
    var elements = new Array(req.body.zone, req.body.docType, req.body.userID, req.body.userMail, req.body.name,req.body.surname, req.body.birthday, req.body.numInst, req.body.docNumber, req.body.scadenzaDoc, req.body.photoByte, req.body.dataPack, req.body.hashData, req.body.hashDocument);
  } else {
    logger.error("Inviati dall'utente campi vuoti");
    res.status(400).send(errorMessage.error1);
    return;
  }
  logger.info("Invio informazioni ad ethereum endpoint");
  requestify.post(globalVariable.urlBlockchain+globalVariable.portBlockchain+globalVariable.restRegistrations, {
        hashData: req.body.hashData,
        hashImage: req.body.hashDocument
    })
    .then(function(response) {
        logger.info("Ricevuta risposta da ethereum endpoint");
        var result = response.getBody().id;
        elements.push(result);
        dbUtility.make_query(dbUtility.insert_user_query, elements)
        .then(function() {
          serverCallback.post_registration(result, res);
        })
        .catch(function(error) {
          logger.error(error);
          res.status(500).send(errorMessage.error2);
        });
    })
    .catch(function(error) {
      logger.error(error);
      res.status(500).send(errorMessage.error2);
    });
}

function search(req, res, next) {
  logger.info("Ricerca registrazione");
  var tx = req.swagger.params.id.value;
  if(!tx) {
    res.status(400).send(errorMessage.error1);
  }
  requestify.get(globalVariable.urlBlockchain+globalVariable.portBlockchain+globalVariable.restRegistrations+"/"+tx)
  .then(function(response) {
    var result = JSON.stringify(response.getBody());
    console.log(result);
    serverCallback.get_registration(result, res);
  })
  .catch(function(error) {
    logger.error(error);
    res.status(500).send(errorMessage.error2);
  });

}

function uncertificateUser(req, res, next) {
if(req.body.hashData && req.body.hashDocument) {
    var elements = new Array(req.body.hashData, req.body.hashDocument);
  } else {
    logger.error("Inviati campi vuoti");
    res.status(400).send(errorMessage.error1);
    return;
  }
  logger.info("Invio informazioni ad ethereum endpoint");
  requestify.delete(globalVariable.urlBlockchain+globalVariable.portBlockchain+globalVariable.restRegistrations, {
        hashData: req.body.hashData,
        hashImage: req.body.hashDocument
    })
    .then(function(response) {
        logger.info("Ricevuta risposta da ethereum endpoint");
        var result = response.getBody().id;
	console.log(result);
        elements.push(result);
        serverCallback.post_registration(result, res);
    })
    .catch(function(error) {
      logger.error(error);
      res.status(500).send(errorMessage.error2);
    });
}

module.exports = {certificateUser, uncertificateUser, search}
