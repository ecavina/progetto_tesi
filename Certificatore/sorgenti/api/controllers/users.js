'use strict';
  //require
  var globalVariable = require(__dirname + '/../../config/globalVariable.js');
  var dbUtility = require(__dirname + '/../../db/index.js');
  var errorMessage = require(__dirname + '/../../config/errorMessage.js');
  var serverCallback = require(__dirname + '/../../serverUtility/serverCallback.js');
  var validator = require(__dirname + '/../../serverUtility/validationUtility.js');
  var logger = globalVariable.logger;

  module.exports = {emailExist};

  function emailExist(req, res, next) {
    if(validator.validate_email(req.query.mail)){
  		var mail=req.query.mail;
  	}else {
  		logger.error("Email non valida o vuota");
  		res.status(400).send(errorMessage.error3);
  		return;
  	}
  	dbUtility.make_query(dbUtility.select_mail_query, mail)
  	.then(function(result){
   	  serverCallback.db_get_mail(result, res);
    })
    .catch(function(){
      res.status(500).send(errorMessage.error2);
      return;
    });
  }
