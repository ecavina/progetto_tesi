function Foto(byteString) {
  this.byteString = byteString;
}

Foto.prototype = {
  constructor: Foto,
  getByteString:function() {
    return this.byteString;
  }
}
