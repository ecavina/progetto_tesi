function User(zone, doc_type, surname, name, birthday, institution, doc_number, duedate) {
  this.country = zone;
  this.doc_type = doc_type;
  this.surname = surname;
  this.name = name;
  this.birthday = birthday;
  this.institution = institution;
  this.doc_number = doc_number;
  this.duedate = duedate;
}

User.prototype = {
  constructor: User,
  getInfo:function(){
    return this.country + "/" + this.doc_type + "/" + this.surname + "/" + this.name + "/" + this.birthday + "/" + this.institution + "/" + this.doc_number + "/" + this.duedate;
  },
  getCountry:function(){
    return this.country;
  },
  getDocType:function(){
    return this.doc_type;
  },
  getSurname:function(){
    return this.surname;
  },
  getName:function(){
    return this.name;
  },
  getBirthday:function(){
    return this.birthday;
  },
  getInstitution:function(){
    return this.institution;
  },
  getDocNumber:function(){
    return this.doc_number;
  },
  getDueDate:function(){
    return this.duedate;
  }
}
