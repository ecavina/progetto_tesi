function ImolaInReteData(email, username) {
  this.email = email;
  this.username = username;
}

ImolaInReteData.prototype = {
  constructor: ImolaInReteData,
  getInfo:function(){
    return this.email + "/" + this.username;
  },
  getEmail:function(){
    return this.email;
  },
  getUsername:function(){
    return this.username;
  }
}
