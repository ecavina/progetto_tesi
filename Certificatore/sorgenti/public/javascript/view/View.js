function View() {}

View.errorMessage = function(message) {
  $("#output_message").html("<div class='alert alert-danger'><h4><strong>Impossible to proceed</strong>, "+message+"</h4></div>");
}

//Segnala i campi vuoti
View.emptyFields = function(form) {
  $("#output_message").html("<div class='alert alert-danger'><h4><strong>Impossible to proceed<strong>, all fields are required</h4></div>");
  var frm = document.getElementById(form);
  for (i = 0; i < frm.length ;i++) {
      if(!frm.elements[i].value) {
        var id = $(frm.elements[i]).attr('id');
        $("#"+id).css({"border-style":"solid","border-color":"red"});
      }
  }
}
//Attesa transazione
View.waitTransaction = function() {
	$("#next-4").prop("disabled",true);
	$("#before-3").prop("disabled",true);
	$("#step4").css({"display":"none"});
	$("#output_message").html("<div class='alert alert-info'><h4><strong>Loading...</strong></h4><br></div><div id=\"loader\"></div>");
}
//Attesa Database
View.waitDatabase = function() {
  $("#next-2").prop("disabled",true);
  $("#before-1").prop("disabled",true);
  $("#step4").css({"display":"none"});
	$("#output_message").html("<div class='alert alert-info'><h4><strong>Loading...</strong></h4><br></div><div id=\"loader\"></div>");
}
View.unlockButton = function(button) {
  $(button).prop("disabled",false);
}
//Errore quando viene inviato il dato su ethereum
View.ethereumError = function(error) {
	$("#next-4").prop("disabled",false);
	$("#before-3").prop("disabled",false);
  $("#output_message").html("<div class='alert alert-danger'><h4><strong>Impossible to proceed</strong>, "+error+"</h4></div>");
}
//Riassunto
View.summary = function(dataInf, documentInf) {
  $("#output_message").html("<div class='alert alert-info'><h4><strong>Summary:</strong></h4><br><strong>Data pack: </strong>"+dataInf+"<br><br><img style=\"max-width: 100%;\" alt=\"photo image\" src=\""+documentInf+"\"</div>");
}

View.fromToStep = function(from, to) {
  $("#step-"+from).css({"display":"none"});
  $("#step-"+to).css({"display":"block"});
  $("#output_message").html("");
  if (to == 5) {
    $("#loader").css({"display":"none"});
  }
}
