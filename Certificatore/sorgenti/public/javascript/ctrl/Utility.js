function Utility() {}

Utility.isValidEmailAddress = function(emailAddress, pattern) {
  return pattern.test(emailAddress);
};

Utility.photoCheck = function() {
  //check whether browser fully supports all File API
  if (window.File && window.FileReader && window.FileList && window.Blob) {
    var bool = false;
    //get the file size and file type from file input field
    var fsize = $('#photo_file')[0].files[0].size;
    var ftype = $('#photo_file')[0].files[0].type;
    var fname = $('#photo_file')[0].files[0].name;

    if (fsize>10048576) {
      bool = false;
    } else{
      bool = true;
    }

    switch(ftype) {
      case 'image/png':
      case 'image/gif':
      case 'image/jpeg':
      case 'image/pjpeg':
          bool = true;
          break;
      default:
          bool = false;
    }
  } else {
    alert("Browser obsoleto");
    return false;
  }
  return bool;
};

Utility.isValidDate = function(dateString) {
  // First check for the pattern
  if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
    return false;

  // Parse the date parts to integers
  var parts = dateString.split("/");
  var day = parseInt(parts[1], 10);
  var month = parseInt(parts[0], 10);
  var year = parseInt(parts[2], 10);

  // Check the ranges of month and year
  if(year < 1000 || year > 3000 || month == 0 || month > 12)
      return false;

  var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

  // Adjust for leap years
  if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
      monthLength[1] = 29;

  // Check the range of the day
  return day > 0 && day <= monthLength[month - 1];
};

Utility.jsDataToPhp = function(data) {
  var d = new Date(data),
  dformat = [d.getFullYear() ,d.getMonth()+1,
             d.getDate()
             ].join('-')+' '+
            [d.getHours(),
             d.getMinutes(),
             d.getSeconds()].join(':');
  return dformat;
}

Utility.concatenateInfo = function(next){
	  window.dataInfo = "[Zone:"+window.user.getCountry()
      +"/DocumentType:"+window.user.getDocType()
      +"/Company:localfocus"
      +"/Service:imolainrete"
      +"/UserID:"+window.ImolaInReteInfo.getUsername()
      +"/UserEmail:"+window.ImolaInReteInfo.getEmail()
      +"/Data1:"+window.user.getSurname()
      +"/Data2:"+window.user.getName()
      +"/Data3:"+window.user.getBirthday()
      +"/Data4:"+window.user.getInstitution()
      +"/Data5:"+window.user.getDocNumber()
      +"/Data6:"+window.user.getDueDate()+"]";

      var shaObj = new jsSHA("SHA-1", "TEXT");
      shaObj.update(window.dataInfo);
      window.hashData = shaObj.getHash("HEX");
      documentInfo = photo.getByteString();
      var shaObj = new jsSHA("SHA-1", "TEXT");
      shaObj.update(window.documentInfo);
      window.hashDocument = shaObj.getHash("HEX");
      
      next();
}
