function DBControl() {}

  /*
  Questa funzione preparare la richiesta e fa una richiesta ajax al server nodejs
  Tramite post richiama la funzione nel server che si occuperà di controllare l'esistenza
  nel database delle email certificate se è già tale
  */
  DBControl.emailExist = function(email, username, func) {
    var data = {
      "mail":email
    }
    $.ajax({
      type: 'GET',
      dataType: "json",
      url: globalVar.RESTUSERS+"/mail",
      data: data,
      success: function(response) {
        if(response.response == "true") {
          func(true);
        } else {
          func(false);
        }
        unlockMailButton();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        View.errorMessage("Error: "+jqXHR.status+" - "+jqXHR.responseText);
        unlockMailButton();
      }
    });
    View.waitDatabase();
  }

  function unlockMailButton() {
    View.unlockButton("#next-2");
    View.unlockButton("#before-1");
  }

  DBControl.insertInDb = function(idTx, func) {
	var bdate = Utility.jsDataToPhp(window.user.getBirthday());
    var dudate = Utility.jsDataToPhp(window.user.getDueDate());

    var data = {
      "zone":window.user.getCountry(),
      "docType":window.user.getDocType(),
      "userID":window.ImolaInReteInfo.getUsername(),
      "userMail":window.ImolaInReteInfo.getEmail(),
      "name":window.user.getName(),
      "surname":window.user.getSurname(),
      "birthday":bdate,
      "numInst":window.user.getInstitution(),
      "docNumber":window.user.getDocNumber(),
      "scadenzaDoc":dudate,
      "photoByte":window.documentInfo,
      "dataPack":window.dataInfo,
      "hashData":window.hashData,
      "hashDocument":window.hashDocument,
      "idTx":idTx
    }


    $.ajax({
      type: 'POST',
      dataType: "json",
      url: globalVar.RESTDB,
      data: data,
      success: function(response) {
        func(response.response);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        View.errorMessage("Error: "+jqXHR.status+" - "+jqXHR.responseText);
      }
    });
  }
