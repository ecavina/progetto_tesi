function BlockchainControl() {}

/*
Questa funzione invia al server il pacchetto di dati da inserire nella blockchain
*/
BlockchainControl.sendToEthereum = function() {
  var bdate = Utility.jsDataToPhp(window.user.getBirthday());
  var dudate = Utility.jsDataToPhp(window.user.getDueDate());

  var data = {
    "zone":window.user.getCountry(),
    "docType":window.user.getDocType(),
    "userID":window.ImolaInReteInfo.getUsername(),
    "userMail":window.ImolaInReteInfo.getEmail(),
    "name":window.user.getName(),
    "surname":window.user.getSurname(),
    "birthday":bdate,
    "numInst":window.user.getInstitution(),
    "docNumber":window.user.getDocNumber(),
    "scadenzaDoc":dudate,
    "photoByte":window.documentInfo,
    "dataPack":window.dataInfo,
    "hashData":window.hashData,
    "hashDocument":window.hashDocument,
  }
  $.ajax({
    type: 'POST',
    contentType: 'application/json',
    dataType: "json",
    url: globalVar.RESTTRANSACTIONS,
    data: JSON.stringify(data),
    success: function(response) {
      View.fromToStep(4,5);
      BlockchainControl.getTx(response.result);
    },
    error: function(jqXHR, textStatus, errorThrown) {
      var res = jqXHR.responseText.split(":");
      View.ethereumError("Error: "+jqXHR.status+" - "+res[0]+res[1]);
    }
  });
  View.waitTransaction();
}

  /*
  Questa funzione permette di riottenere le informazioni che sono state certificate nella
  blockchain richiamando la funzione apposita nel server
  */
  BlockchainControl.getTx = function(idTx) {
  	var info = {
  		"hash":idTx
  	}
  	$.ajax({
      type: 'GET',
      dataType: "json",
      url: globalVar.RESTTRANSACTIONS+"/"+idTx,
      data: info,
      success: function(response) {
        var input = response.input;
        alert("Nella campo input della transazione vi sono queste informazioni: " + input);
      },
      error: function(jqXHR, textStatus, errorThrown) {
        var res = jqXHR.responseText.split(":");
        View.ethereumError("Error: "+jqXHR.status+" - "+res[0]+res[1]);
      }
    });
  }
