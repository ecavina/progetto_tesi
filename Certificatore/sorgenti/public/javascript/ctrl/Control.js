$(function(){
  $("#birthdatepicker").datepicker({
    changeMonth : true,
		changeYear : true,
    yearRange: "-100:+0"
  }); //Datepicker di jquery
  $("#duedatepicker").datepicker({
    changeMonth : true,
		changeYear : true,
    yearRange: "-100:+20"
  }); //Datepicker di jquery

  //Tasto next prima schermata
  $("#next-1").click(function() {
    var zone = $("#zone").val();
    var name = $("#name").val();
    var surname = $("#surname").val();
    var birthday = $("#birthdatepicker").val();
    var institution = $("#institution").val();
    var docType = $("#doc_type").val();
    var docNumber = $("#doc_number").val();
    var duedate = $("#duedatepicker").val();
    var check = validateFirstForm(zone, name, surname, institution, birthday, docType, docNumber, duedate);
    if(!check) {
      return false;//Se ci sono problemi nella form interrompe
    }
    window.user = new User(zone, docType, surname, name, birthday, institution, docNumber, duedate);
    View.fromToStep(1,2);
  })
  //Tasto next seconda schermata
  $("#next-2").click(function() {
    var email = $("#mail").val();
    var username = $("#username").val();
    if(email && username) {
      if(Utility.isValidEmailAddress(email, window.mailRegExp)) {
        DBControl.emailExist(email, username, function(result) {
          if(!result) {
            createUserObjects(email, username);
            View.fromToStep(2,3);
          } else {
            View.errorMessage("Mail is already certified");
          }
        });
      } else {
        View.errorMessage("The mail address you entered is invalid");
        return false;
      }
    } else {
      View.emptyFields("form-2");
      return false;
    }
  })
  //Tasto next terza schermata
  $("#next-3").click(function() {
    var image =$("#photo_file").val();
    //alert(image);
    var check;
    if(image) {//Controlla se è stata inserita un immagine o no
      check = Utility.photoCheck();
    } else {
      View.errorMessage("please load an image");
      return false;
    }
    if(!check) {//Controlla se il file inserito è effettivamente un immagine
        View.errorMessage("the uploaded file is not valid");
        return false;
    }
    createPhotoObject();
  })
  //Tasto next schermata 4
  $("#next-4").click(function() {
    BlockchainControl.sendToEthereum();
  })
  //Tasto indietro seconda schermata
  $("#before-1").click(function() {
    View.fromToStep(2,1);
  })
  //Tasto indietro terza schermata
  $("#before-2").click(function() {
    View.fromToStep(3,2);
  })
  //Tasto indietro quarta schermata
  $("#before-3").click(function() {
    View.fromToStep(4,3);
  })

  function validateFirstForm(zone, name, surname, institution, birthday, docType, docNumber, duedate) {
    if(zone && name && surname && birthday && docType && docNumber && duedate && institution) {
      var today = new Date();
      var bddate = new Date(birthday);
      var ddate = new Date(duedate);
      if(!Utility.isValidDate(birthday) || !Utility.isValidDate(duedate)) {
        View.errorMessage("The date format isn't correct");
        return false;
      }
      if(today.getFullYear() - bddate.getFullYear() < 18) {
          maggiorenne = false;
      }
      if(bddate>today) {
        View.errorMessage("Insert a valid birthday date");
        return false;
      }
      if(today>ddate) {
          View.errorMessage("The document has expired");
          return false;
      } else {
        return true;
      }
    } else {
      View.emptyFields("form-1");//Se anche solo un campo è vuoto (passa il nome della form)
      return false;
    }
  }

  function createUserObjects(email, username) {
    ImolaInReteInfo = new ImolaInReteData(email, username);
  }

  function createPhotoObject() {
    var input = document.getElementById("photo_file").files;
    var fileData = new Blob([input[0]]);
    var reader = new FileReader();
    var bytes;
    reader.readAsDataURL(fileData);
    reader.onload = function(){
      window.photo = new Foto(reader.result);
      createDatapack();
    }
  }

  /*
  Prepara il "datapack" di informazioni (esclusa la foto) e lo hasha con SHA-1,
  in seguito hasha anche il file foto
  */
  function createDatapack() {
	  Utility.concatenateInfo(function() {
		View.fromToStep(3,4);
        View.summary(window.dataInfo, window.documentInfo);
	  });
  }

  function insertIntoDatabase(idTx) {
    DBControl.insertInDb(idTx,function(result) {
      if(result === "true") {
        View.fromToStep(4,5);
        BlockchainControl.getTx(idTx);
      }
    });
  }
});
