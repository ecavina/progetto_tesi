var globalVariable = (function() {
	var regexpMail = /^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	var urlBlockchain = "@@urlEth";
	var portBlockchain = "@@portBlockchain";
	var restRegistrations = "@@restRegistrations";
	var pwdAccount = '@@pwdAccountEth';
	var databaseUrl = '@@databaseUrl';
	var databaseUser = '@@databaseUser';
	var databasePw = '@@databasePw';
	var databaseName = '@@databaseName';
	var serverPort = '@@serverPort';
	var sizeFileLimit = '10mb';

	var log4js = require('log4js');
	log4js.loadAppender('file');
	log4js.addAppender(log4js.appenders.file('logs/server.log'), 'file');
	var logger = log4js.getLogger('file');
	logger.setLevel('@@loggerLevel');

	var express = require('express');
	var app = express();
	var bodyParser = require('body-parser');

	module.exports = {
		regexpMail: regexpMail,
		urlBlockchain: urlBlockchain,
		portBlockchain : portBlockchain,
		restRegistrations : restRegistrations,
		pwdAccount: pwdAccount,
		databaseUrl: databaseUrl,
		databaseUser: databaseUser,
		databasePw: databasePw,
		databaseName: databaseName,
		serverPort: serverPort,
		sizeFileLimit: sizeFileLimit,
		logger: logger,
		express: express,
		app: app,
		bodyParser: bodyParser,
	};
})();
