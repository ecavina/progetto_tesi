module.exports = {
  error1 : {
    message: "Some fields are empty or incorrect",
    errorCode: 1,
    additionalInformation : "http://www.domain.com/rest/errorcode/352"
  },
  error2 : {
    message: "Internal Server Error, it's not possibile to proceed",
    errorCode: 2,
    additionalInformation : "http://www.domain.com/rest/errorcode/352"
  },
  error3 : {
    message: "Bad email syntax",
    errorCode: 2,
    additionalInformation : "http://www.domain.com/rest/errorcode/352"
  }
}
