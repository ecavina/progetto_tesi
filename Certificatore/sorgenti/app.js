'use strict';
var globalVariable = require(__dirname + '/config/globalVariable.js');
var dbUtility = require(__dirname + '/db/index.js');
//var ethUtility = require(__dirname + '/eth/index.js');
var errorMessage = require(__dirname + '/config/errorMessage.js');
var serverCallback = require(__dirname + '/serverUtility/serverCallback.js');
var logger = globalVariable.logger;
var SwaggerExpress = require('swagger-express-mw');
var app = require('express')();

module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

dbUtility.init_db();

app.use(globalVariable.bodyParser.json({limit: globalVariable.sizeFileLimit}));
app.use(globalVariable.bodyParser.raw({limit: globalVariable.sizeFileLimit}));

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) {
    logger.error("Errore durante la preparazione di swagger");
    throw err;
  }

  // install middleware
  swaggerExpress.register(app);

  var port = parseInt(globalVariable.serverPort) || 10010;

  app.use(logRequest);
  app.use(globalVariable.express.static(__dirname + '/public'));

  app.listen(port);

  logger.info('Server is running on port: ' + port);
});

//Runs every time a request is received
function logRequest(req, res, next) {
    logger.info('Request from: ' + req.ip + ' For: ' + req.path);
    next();
}
