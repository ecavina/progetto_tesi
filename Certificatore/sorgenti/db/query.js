var string = {
  insert_user: 'INSERT INTO User (zone, docType, userID, userMail, name, surname, birthday, numInst, docNumber, scadenzaDoc, photoByte, dataPack, hashData, hashDocument, idTx) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
  select_mail: 'SELECT id FROM User WHERE userMail=?'
}
exports.string = string;
