var dbUtil = (function() {
	var mysql = require('mysql');
	var globalVariable = require('../config/globalVariable.js')
	var logger = globalVariable.logger;
	var pool;

	var initDB = function() {
		logger.debug("Inizializzazione connessione database...");
		pool = mysql.createPool({
			host     : globalVariable.databaseUrl,
			user     : globalVariable.databaseUser,
			password : globalVariable.databasePw,
			database : globalVariable.databaseName
		});
		logger.debug("Creazione connessione riuscita");
	};

	var makeQuery = function(query, elements) {
		return new Promise(function(resolve, reject) {
			pool.getConnection(function(err, connection) {
				if(err) {
					logger.error("Impossibile connettersi al database: "+err);
					reject("Fallito");
				} else {
					var queryString = connection.query(query, elements, function(err, rows, fields) {
						if(err) {
							logger.error("Errore durante la query: " + err);
							reject(rows);
						} else {
							logger.debug("Query riuscita");
							resolve(rows);
						}
						connection.release();
					});
					logger.debug("Inviata query al database, QUERY: " + queryString.sql);
				}
			});
		})
	}

	return {
		init_db: initDB,
		make_query: makeQuery
	}
})();
exports.dbUtil = dbUtil;
