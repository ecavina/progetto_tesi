var utility = require(__dirname+'/dbUtility.js').dbUtil;
var query = require(__dirname+'/query.js');
module.exports = {
  init_db: utility.init_db,
  make_query: utility.make_query,
  insert_user_query: query.string.insert_user,
  select_mail_query: query.string.select_mail
}
