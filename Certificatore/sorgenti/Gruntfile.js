module.exports = function(grunt) {
  var envTarget = grunt.option('env') || 'local';
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    devSettings: grunt.file.readJSON('gruntConfig/gruntDevConfig.json'),
    prodSettings: grunt.file.readJSON('gruntConfig/gruntProdConfig.json'),
    testSettings: grunt.file.readJSON('gruntConfig/gruntTestConfig.json'),
    config: {
      dev: {
        options: {
          variables: {
            root:  '<%- devSettings.root %>',
            restRegistrations: '<%- devSettings.restRegistrations %>',
            restDB: '<%- devSettings.restDb %>',
            urlEth:'<%- devSettings.urlEth %>',
            portBlockchain:'<%- devSettings.portBlockchain %>',
            pwdAccountEth:'<%- devSettings.pwdAccountEth %>',
            databaseUrl:'<%- devSettings.databaseUrl %>',
            databaseUser:'<%- devSettings.databaseUser %>',
            databasePw:'<%- devSettings.databasePw %>',
            databaseName:'<%- devSettings.databaseName %>',
            serverPort:'<%- devSettings.serverPort %>',
            loggerLevel: '<%- devSettings.loggerLevel %>',
          }
        }
      },
      prod: {
        options: {
          variables: {
            root:  '<%- prodSettings.root %>',
            restRegistrations: '<%- prodSettings.restRegistrations %>',
            restDB: '<%- prodSettings.restDb %>',
            urlEth:'<%- prodSettings.urlEth %>',
            portBlockchain:'<%- prodSettings.portBlockchain %>',
            pwdAccountEth:'<%- prodSettings.pwdAccountEth %>',
            databaseUrl:'<%- prodSettings.databaseUrl %>',
            databaseUser:'<%- prodSettings.databaseUser %>',
            databasePw:'<%- prodSettings.databasePw %>',
            databaseName:'<%- prodSettings.databaseName %>',
            serverPort:'<%- prodSettings.serverPort %>',
            loggerLevel: '<%- prodSettings.loggerLevel %>',
          }
        }
      },
      test: {
        options: {
          variables: {
            root:  '<%- testSettings.root %>',
            restRegistrations: '<%- testSettings.restRegistrations %>',
            restDB: '<%- testSettings.restDb %>',
            urlEth:'<%- testSettings.urlEth %>',
            portBlockchain:'<%- testSettings.portBlockchain %>',
            pwdAccountEth:'<%- testSettings.pwdAccountEth %>',
            databaseUrl:'<%- testSettings.databaseUrl %>',
            databaseUser:'<%- testSettings.databaseUser %>',
            databasePw:'<%- testSettings.databasePw %>',
            databaseName:'<%- testSettings.databaseName %>',
            serverPort:'<%- testSettings.serverPort %>',
            loggerLevel: '<%- testSettings.loggerLevel %>',
          }
        }
      },
    },
    includeSource: {
      options: {
        basePath: ['build/public/javascript', 'build/public/javascript/ctrl', 'build/public/javascript/view', 'build/public/javascript/model'],
        baseUrl: 'javascript/',
      },
      myTarget: {
        files: {
          'build/public/index.html': 'public/index.html'
        }
      }
    },
    replace: {
      globalVarClient: {
        options: {
          patterns: [{
            match: 'root',
            replacement: '<%= grunt.config.get("root") %>'
          },
          {
            match: 'usersUrl',
            replacement: '<%= grunt.config.get("restDB") %>'
          },
          {
            match: 'transactionsUrl',
            replacement: '<%= grunt.config.get("restRegistrations") %>'
          }
          ]
        },
        src: 'public/javascript/globalVariable.js',
        dest: 'build/public/javascript/globalVariable.js'
      },
      globalVarServer: {
        options: {
          patterns: [{
            match: 'urlEth',
            replacement: '<%= grunt.config.get("urlEth") %>'
          },
          {
            match: 'pwdAccountEth',
            replacement: '<%= grunt.config.get("pwdAccountEth") %>'
          },
          {
            match: 'databaseUrl',
            replacement: '<%= grunt.config.get("databaseUrl") %>'
          },
          {
            match: 'databaseUser',
            replacement: '<%= grunt.config.get("databaseUser") %>'
          },
          {
            match: 'databasePw',
            replacement: '<%= grunt.config.get("databasePw") %>'
          },
          {
            match: 'databaseName',
            replacement: '<%= grunt.config.get("databaseName") %>'
          },
          {
            match: 'serverPort',
            replacement: '<%= grunt.config.get("serverPort") %>'
          },
          {
            match: 'portBlockchain',
            replacement: '<%= grunt.config.get("portBlockchain") %>',
          },
          {
            match: 'restRegistrations',
            replacement: '<%= grunt.config.get("restRegistrations") %>',
          },
          {
            match: 'loggerLevel',
            replacement: '<%= grunt.config.get("loggerLevel") %>',
          }
          ]
        },
        src:'config/globalVariable.js',
        dest: 'build/config/globalVariable.js'
      },
    },
    uglify: {
      prod : {
        src : ['public/javascript/ctrl/*.js', 'public/javascript/model/*.js', 'public/javascript/view/*.js'],
        dest : 'build/public/javascript/build.js'
      },
      dev : {

      },
      test: {

      },
      compose: {

      }
    },
    copy: {
      main: {
        files: [
          {expand: true, src: ['public/*'], dest: 'build/', filter: 'isFile'},
          {expand: true, src: ['public/style/**/*.*'], dest: 'build/'},
          {expand: true, src: ['public/lib/jsSHA-2.2.0/**/*.*'], dest: 'build/'},
          {expand: true, src: ['public/lib/log4javascript/**/*.*'], dest: 'build/'},
          {extend: true, src: ['backup_DB/*.*'], dest: 'build/'},
          {expand: true, src: ['scripts/*.*'], dest: 'build/'},
          {expand: true, src: [
            'api/**/*.*', 'config/*.*',
            'db/*.*', 'eth/*.*',
            'logs/*.*',
            'node_modules/**/*.*',
            'serverUtility/serverCallback.js',
            'serverUtility/validationUtility.js',
            'app.js',
            'package.json'
          ], dest: 'build/'},
        ],
      },
      dev: {
        files: [
          {expand: true, src: ['public/javascript/**/*.*'], dest: 'build/'},
        ]
      },
      compose: {
        files: [
          {expand: true, src: ['public/javascript/**/*.*'], dest: 'build/'},
        ]
      },
      prod: {

      },
      test: {
        files: [
          {expand: true, src: ['ethereum.json', 'test/**/*.*',], dest: 'build/'},
        ]
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-config');
  grunt.loadNpmTasks('grunt-include-source');
  grunt.registerTask('build', ['config:'+envTarget, 'uglify:'+envTarget, 'copy:main', 'copy:'+envTarget, 'replace', 'includeSource']);
};
