'use strict';
var chai = require('chai');
var ZSchema = require('z-schema');
var customFormats = module.exports = function(zSchema) {
  // Placeholder file for all custom-formats in known to swagger.json
  // as found on
  // https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#dataTypeFormat

  var decimalPattern = /^\d{0,8}.?\d{0,4}[0]+$/;

  /** Validates floating point as decimal / money (i.e: 12345678.123400..) */
  zSchema.registerFormat('double', function(val) {
    return !decimalPattern.test(val.toString());
  });

  /** Validates value is a 32bit integer */
  zSchema.registerFormat('int32', function(val) {
    // the 32bit shift (>>) truncates any bits beyond max of 32
    return Number.isInteger(val) && ((val >> 0) === val);
  });

  zSchema.registerFormat('int64', function(val) {
    return Number.isInteger(val);
  });

  zSchema.registerFormat('float', function(val) {
    // should parse
    return Number.isInteger(val);
  });

  zSchema.registerFormat('date', function(val) {
    // should parse a a date
    return !isNaN(Date.parse(val));
  });

  zSchema.registerFormat('dateTime', function(val) {
    return !isNaN(Date.parse(val));
  });

  zSchema.registerFormat('password', function(val) {
    // should parse as a string
    return typeof val === 'string';
  });
};

customFormats(ZSchema);

var validator = new ZSchema({});
var request = require('request');
var expect = chai.expect;

describe('/registrations', function() {
  describe('post', function() {
    it('POST REGISTRAZIONE: should respond with 200 Success', function(done) {
      this.timeout(5000);
      /*eslint-disable*/
      var schema = {
        "required": [
          "result"
        ],
        "properties": {
          "result": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        json: {
          "zone":"eu",
          "docType":"tessera_sanitaria",
          "userID":"ecavina",
          "userMail":new Date().getTime()+"@test.it",
          "name":"eugenio",
          "surname":"cavina",
          "birthday":"2016-11-07",
          "numInst":"506486",
          "docNumber":"fasdfecvaefea",
          "scadenzaDoc":"2050-11-07",
          "photoByte":"00000",
          "dataPack":"hyfytrdxuiuo",
          "hashData":"hashfinto",
          "hashDocument":"hasfinto",
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        expect(res.statusCode).to.equal(200);

        expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

    it('POST REGISTRAZIONE: should respond with 400 Error', function(done) {
      /*eslint-disable*/
      var schema = {
        "required": [
          "message"
        ],
        "properties": {
          "message": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        json: {
          data: ''
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        expect(res.statusCode).to.equal(400);

        expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

    it('POST REGISTRAZIONE: should respond with 500 Error', function(done) {
      /*eslint-disable*/
      var schema = {
        "required": [
          "message"
        ],
        "properties": {
          "message": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        json: {
          data: 'DATA GOES HERE'
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        //expect(res.statusCode).to.equal(500);

        expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

  });

  describe('delete', function() {
    it('DELETE REGISTRAZIONE: should respond with 200 Success', function(done) {
      /*eslint-disable*/
      var schema = {
        "required": [
          "result"
        ],
        "properties": {
          "result": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },
	json: {
         "hashData":"asdfsadfasdfsafd",
	"hashDocument":"asdfasdfsafd"
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        //expect(res.statusCode).to.equal(200);

        //expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

    it('DELETE REGISTRAZIONE: should respond with 400 Error', function(done) {
      /*eslint-disable*/
      var schema = {
        "required": [
          "message"
        ],
        "properties": {
          "message": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        expect(res.statusCode).to.equal(400);

        expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

    it('DELETE REGISTRAZIONE: should respond with 500 Error', function(done) {
      /*eslint-disable*/
      var schema = {
        "required": [
          "message"
        ],
        "properties": {
          "message": {
            "type": "string"
          }
        }
      };

      /*eslint-enable*/
      request({
        url: 'http://localhost:10010/registrations',
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        }
      },
      function(error, res, body) {
        if (error) return done(error);

        //expect(res.statusCode).to.equal(500);

        expect(validator.validate(body, schema)).to.be.true;
        done();
      });
    });

  });

});
