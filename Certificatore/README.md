## **Certificatore**

Modulo NodeJS per la certificazione.

Comunica con un database MySQL il cui dump si trova in: *backup_DB*. Questo dump può essere sostituito con un qualunque backup, infatti il modulo NodeJS si occuperà di fare il restore del database tramite il file che si troverà in questa directory.

Nel contesto di esecuzione dell'ambiente, il database viene istanziato con Docker tramite il docker file all'interno della directory *DBMYSQL/*

