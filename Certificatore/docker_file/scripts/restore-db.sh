#!/bin/bash
#
# Restore db
#

# indirizzo ip del container mysql
ip=iir-mysql

echo "Executing script init-and-restore-DB.sh...."

# attendo che il servizio sia disponibile sulla porta 3306

until nc -z -v -w30 $ip 3306 
do 
  echo "Waiting for DB connection...." 
  sleep 5 
done 

# crea un utente guest remoto
echo "Creating new remote user...."

mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD -h $ip -e "create database CertificatedUser;"

# restore del backup database
echo "Restoring database backup...."

mysql -u $MYSQL_ROOT_USER -p$MYSQL_ROOT_PASSWORD -h $ip  CertificatedUser < /mnt/sources/Certificatore/backup_DB/CertificatedUser.sql
