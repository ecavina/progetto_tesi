#!/bin/bash
#
# inizializza server
#

echo "Environment initialization...."

if [ $CHANNEL_BUILD == "dev" ]; then

/mnt/sources/Certificatore/docker_file/scripts/restore-db.sh
cd /mnt/sources/Certificatore/sorgenti/
npm run build_dev
cd /mnt/sources/Certificatore/sorgenti/build/
node app.js

elif [ $CHANNEL_BUILD == "prod" ]; then

cd /mnt/sources/Certificatore/sorgenti/
npm run build_prod

elif [ $CHANNEL_BUILD == "test" ]; then

/mnt/sources/Certificatore/docker_file/scripts/restore-db.sh
cd /mnt/sources/Certificatore/sorgenti/
npm run build_test
cd /mnt/sources/Certificatore/sorgenti/build/
npm run start_fake_server &
npm run swagger_test

fi



