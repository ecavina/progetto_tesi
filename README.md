#**Certificatore d'identità digitale**

Il progetto è un sistema che espone dei servizi REST di certificazione dell'identità di una persona. Il sistema garantisce la certificazione comunicando con la Blockchain Ethereum.

I servizi vengono esposti dal componente del sistema chiamato *Certificatore*, che a sua volta comunica con un secondo componente chiamato *EthereumEndpoint*, il quale dialoga direttamente con un nodo della Blockchain. 

A supporto del Certificatore è presente un database MySQL, il quale registra le certificazioni degli utenti.

Nella directory di progetto si trova *run.sh*, lo script che permette di eseguire le varie operazioni sul progetto, come per esempio build ed esecuzione.
Per quanto riguarda l'esecuzione dell'ambiente viene utilizzato Docker-compose. Il file docker-compose originale è all'interno della directory apposita (dockercompose_file/). Eventuali modifiche andranno effettuate sul file originale e non su quello creato dallo script di avvio.

Gli ambienti previsti sono:

DEV -> per lo sviluppo

TEST -> per i test automatici dei servizi

PROD -> per la produzione (questa profilo non prevede l'esecuzione dell'ambiente)

Le configurazioni per la build dei componenti del progetto si trova nelle seguenti directory:

*Certificatore/sorgenti/gruntConfig/*
*EthereumEndpoint/sorgenti/gruntConfig/*


--------------------

##**Installazioni necessarie**

UBUNTU 16
GIT, DOCKER, NODEJS


GIT

installare git

git clone https://ecavina@bitbucket.org/ecavina/progetto_tesi.git


DOCKER

sudo apt-get update

sudo apt-get install apt-transport-https ca-certificates

 sudo apt-key adv \
               --keyserver hkp://ha.pool.sks-keyservers.net:80 \
               --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual

sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

sudo apt-get update

sudo apt-get install docker-engine

sudo service docker start

-------
pulire ambiente da volume:
	sudo docker volume rm $(sudo docker volume ls -qf dangling=true)

DOCKER-COMPOSE

sudo curl -o /usr/local/bin/docker-compose -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)"

sudo chmod +x /usr/local/bin/docker-compose

docker-compose -v




NODEJS

sudo apt-get install nodejs

sudo apt-get install npm

sudo ln -s "$(which nodejs)" /usr/bin/node

sudo npm install -g swagger


